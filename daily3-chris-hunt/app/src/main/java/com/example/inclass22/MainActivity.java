package com.example.inclass22;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView mSearchResultsDisplay;
    private EditText mSearchTermEditText;
    private Button mSearchButton;
    private Button mResetButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // connect with the visual elements in activity_main.xml
        mSearchResultsDisplay = (TextView) findViewById(R.id.tv_display_text);
        mSearchTermEditText = (EditText) findViewById(R.id.et_search_box);
        mSearchButton = (Button) findViewById(R.id.search_button);
        mResetButton = (Button) findViewById(R.id.reset_button);

        final String[] toolBox = {"Hammer", "Screwdriver", "Wrench", "Scissors", "Tape Measure", "Saw", "Pliers"};
        for (String tool : toolBox){
            mSearchResultsDisplay.append("\n\n"+tool);
        }

        final String defaultDisplayText = mSearchResultsDisplay.getText().toString();
        // responding to search button
        mSearchButton.setOnClickListener(
                new View.OnClickListener(){
                    public void onClick(View v){
                        // get search string from user
                        String searchText = mSearchTermEditText.getText().toString();
                        for(String tool : toolBox){
                            if(tool.toLowerCase().equals(searchText.toLowerCase())){
                                mSearchResultsDisplay.setText(tool);
                                break;
                            }else{
                                mSearchResultsDisplay.setText("No results match");
                            }
                        }
                    }
                }
        ); // end

        mResetButton.setOnClickListener(
                new View.OnClickListener(){
                    public void onClick(View v){
                        mSearchResultsDisplay.setText(defaultDisplayText);

                    }
                }
        ); // end
    } // end of onCreate
} // end of MainActivity class